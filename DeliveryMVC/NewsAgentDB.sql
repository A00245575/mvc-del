DROP DATABASE IF EXISTS NewsAgentDB;
CREATE DATABASE IF NOT EXISTS NewsAgentDB;
USE NewsAgentDB;

SELECT 'CREATING PUBLICATION TABLE' as 'INFO';

DROP TABLE IF EXISTS publication;

CREATE TABLE publication (
		pub_id INTEGER AUTO_INCREMENT,
		title VARCHAR(45) NOT NULL,
		price DECIMAL(5,2) NOT NULL,
		type VARCHAR(45) NOT NULL,
		frequency TEXT(20) NOT NULL,
		stock INT NOT NULL,
		PRIMARY KEY (pub_id));
		
SELECT 'INSERTING DATA INTO TABLE PUBLICATION' as 'INFO';

INSERT INTO publication VALUES(null, 'Sunday Times', 2.99, 'Newspaper', 'Weekly', 200);
INSERT INTO publication VALUES(null, 'Sunday Independant', 2.29, 'Newspaper',  'Weekly', 200);
INSERT INTO publication VALUES(null, 'Times Magazine', 4.99, 'Magazine'  'Monthly',, 200);
INSERT INTO publication VALUES(null, 'Daily Star', 1.49, 'Newspaper',  'Daily', 200);
INSERT INTO publication VALUES(null, 'National Geographic', 2.99, 'Newspaper', 'Monthly', 200);

SELECT 'CHECKING TO SEE IF DATA INSERTED TO PUBLICATION CORRECTLY' as 'INFO';

SELECT * FROM publication;

SELECT 'CREATING REGION TABLE' as 'INFO';

DROP TABLE IF EXISTS region;

CREATE TABLE region(
		reg_id INTEGER AUTO_INCREMENT,
		description VARCHAR(45) NOT NULL,
		 PRIMARY KEY (reg_id));
		 
SELECT 'INSERTING DATA INTO TABLE REGION' as 'INFO';

INSERT INTO region VALUES(null, 'East Athlone');
INSERT INTO region VALUES(null, 'West Athlone');
INSERT INTO region VALUES(null, 'North Athlone');
INSERT INTO region VALUES(null, 'South Athlone');

SELECT 'CHECKING TO SEE IF DATA INSERTED TO REGION CORRECTLY' as 'INFO';

SELECT * FROM region;

SELECT 'CREATING CUSTOMERS TABLE' as 'INFO';

DROP TABLE IF EXISTS customers;

CREATE TABLE customers(
		cus_id INTEGER  AUTO_INCREMENT,
		reg_id INTEGER NOT NULL,
		first_name VARCHAR(45) NOT NULL,
		last_name VARCHAR(45) NOT NULL,
		dob DATETIME NOT NULL,
		address_line1 VARCHAR(45) NOT NULL,
		address_line2 VARCHAR(45) NOT NULL,
		eirCode VARCHAR(45) NOT NULL,
		PRIMARY KEY (cus_id),
		FOREIGN KEY (reg_id) REFERENCES region (reg_id));
		
SELECT 'INSERTING DATA INTO TABLE CUSTOMERS' as 'INFO';

INSERT INTO customers VALUES(null, 1, 'Paul', 'Armstrong', '1976-09-09', '13 Old Lane', 'Mill Point', 'N65 EW21');
INSERT INTO customers VALUES(null, 2, 'Mark', 'Armstrong', '1986-02-05', '1 Pine Avenue', 'Yellow Way', 'N64 TY65');
INSERT INTO customers VALUES(null, 3, 'Matthew', 'Armstrong', '1984-03-05', '17 Glenn Terrace', 'Ardmore', 'N66 VR45');
INSERT INTO customers VALUES(null, 4, 'Luke', 'Armstrong', '1945-19-01', '45 Avenue Street', 'Springfield', 'N63 WQ23');
INSERT INTO customers VALUES(null, 1, 'John', 'Armstrong', '1971-07-12', '9 Sea Street', 'Bremen', 'N65 ER78');

SELECT 'CHECKING TO SEE IF DATA INSERTED TO CUSTOMERS CORRECTLY' as 'INFO';

SELECT * FROM customers;

SELECT 'CREATING SUBSCRIBES TABLE' as 'INFO';

DROP TABLE IF EXISTS subscribes;

CREATE TABLE subscribes(
	sub_id INTEGER AUTO_INCREMENT,
	cus_id INTEGER NOT NULL,
	pub_id INTEGER NOT NULL,
	ord_date DATETIME NOT NULL,
	PRIMARY KEY (sub_id),
	FOREIGN KEY (cus_id) REFERENCES customers(cus_id),
	FOREIGN KEY (pub_id) REFERENCES publication(pub_id)
	);
  
  SELECT 'INSERTING DATA INTO TABLE SUBSCRIBES' as 'INFO';
  
  INSERT INTO subscribes VALUES(null, 1, 1, '2018-04-06');
  
  SELECT 'CHECKING TO SEE IF DATA INSERTED TO SUBSCRIBES CORRECTLY' as 'INFO';

SELECT * FROM subscribes;

SELECT 'CREATING DRIVER TABLE' as 'INFO';

DROP TABLE IF EXISTS driver;

CREATE TABLE driver(
	driver_id INTEGER AUTO_INCREMENT,
	first_name VARCHAR(45) NOT NULL,
	last_name VARCHAR(45) NOT NULL,
	dob DATETIME NOT NULL,
	phoneNo VARCHAR(45) NOT NULL,
	  PRIMARY KEY (driver_id));
	  
SELECT 'INSERTING DATA INTO TABLE DRIVER' as 'INFO';

INSERT INTO driver VALUES(null, 'John', 'Brophy', '1988-05-13', '085-8764321');
INSERT INTO driver VALUES(null, 'Salmmon', 'O Shea', '1968-02-15', '085-6589346');
INSERT INTO driver VALUES(null, 'Mary', 'Brophy', '1986-07-23', '085-6945434');
INSERT INTO driver VALUES(null, 'Patrica', 'Timothy', '1973-12-06', '087-7596487');

SELECT 'CHECKING TO SEE IF DATA INSERTED TO DRIVER CORRECTLY' as 'INFO';

SELECT * FROM driver;

SELECT 'CREATING RUNS TABLE' as 'INFO';

DROP TABLE IF EXISTS runs;

CREATE TABLE runs(
	driver_id INTEGER NOT NULL,
	reg_id INTEGER NOT NULL,
	FOREIGN KEY(driver_id) REFERENCES driver(driver_id),
	FOREIGN KEY(reg_id) REFERENCES region(reg_id));

SELECT 'INSERTING DATA INTO TABLE RUNS' as 'INFO';

INSERT INTO runs VALUES(1, 1);
INSERT INTO runs VALUES(2, 2);
INSERT INTO runs VALUES(3, 3);
INSERT INTO runs VALUES(4, 4);

SELECT 'CHECKING TO SEE IF DATA INSERTED TO RUNS CORRECTLY' as 'INFO';

SELECT * FROM runs;

SELECT 'CREATING DELIVERY TABLE' as 'INFO';

DROP TABLE IF EXISTS delivery;

CREATE TABLE delivery(
delivery_id INTEGER AUTO_INCREMENT,
cus_id INTEGER NOT NULL,
del_date DATETIME NOT NULL,

PRIMARY KEY(delivery_id),
FOREIGN KEY(cus_id) REFERENCES customers(cus_id));

SELECT 'INSERTING DATA INTO TABLE DELIVERY' as 'INFO';

INSERT INTO delivery VALUES(null, 1, '2018-04-07');

SELECT 'CHECKING TO SEE IF DATA INSERTED TO DELIVERY CORRECTLY' as 'INFO';

SELECT * FROM delivery;

SELECT 'CREATING INVOICE TABLE' as 'INFO';

DROP TABLE IF EXISTS invoice;

CREATE TABLE invoice(
inv_id INTEGER AUTO_INCREMENT,
cus_id INTEGER NOT NULL,
inv_date DATETIME NOT NULL,
PRIMARY KEY(inv_id),
FOREIGN KEY(cus_id) REFERENCES Customers(cus_id));

SELECT 'INSERTING DATA INTO TABLE INVOICE' as 'INFO';

INSERT INTO invoice VALUES(null, 1, '2018-05-01');

SELECT 'CHECKING TO SEE IF DATA INSERTED TO INVOICE CORRECTLY' as 'INFO';

SELECT * FROM invoice;

SELECT 'CREATING RECEIVES_INV TABLE' as 'INFO';

DROP TABLE IF EXISTS receives_inv;

CREATE TABLE receives_inv(
delivery_id INTEGER NOT NULL,
inv_id INTEGER NOT NULL,
FOREIGN KEY(delivery_id) REFERENCES delivery(delivery_id),
FOREIGN KEY(inv_id) REFERENCES invoice(inv_id));

SELECT 'INSERTING DATA INTO TABLE RECEIVES_INV' as 'INFO';

INSERT INTO receives_inv VALUES(1, 1);

SELECT 'CHECKING TO SEE IF DATA INSERTED TO RECEIVES_INV CORRECTLY' as 'INFO';

SELECT * FROM receives_inv;