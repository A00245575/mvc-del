package mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import mvc.Customers.CustomersController;
import mvc.Delivery.DeliveryController;
import mvc.Invoice.InvoiceController;
import mvc.Publications.PublicationsController;
import mvc.Staff.StaffController;
import mvc.Subscription.SubscriptionController;

public class MainController {
	
	private MainView mView = new MainView();
	
	public MainController()
	{
		mView.init();
		new Delivery();
		new Customers();
		new Staff();
		new Subscription();
		new Invoice();
		new Exit();
		new Info();
		new publications();
	}
	
	 class publications implements ActionListener {

		public publications() {
			mView.addPublicationsActionListener(this);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			new PublicationsController();
		}
		 
	 }
	 
	 class Delivery implements ActionListener {
	    	
	    	public Delivery()
	    	{
	    		mView.addDelActionListener(this);
	    	}

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				new DeliveryController();
			}
	 }
	 
	 class Customers implements ActionListener {
		 
		 public Customers()
		 {
			 mView.addCustomersActionListener(this);
		 }

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			//new CustomersController();
			new CustomersController();
		}
		 
		 
	 }
	 
	 class Staff implements ActionListener {
		 
		 public Staff()
		 {
			 mView.addStaffActionListener(this);
		 }

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			new StaffController();
		}
		 
		 
	 }

	 class Subscription implements ActionListener {
	 
	 public Subscription()
	 {
		 mView.addSubActionListener(this);
	 }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		new SubscriptionController();
	}
	 
	 
}

	 class Invoice implements ActionListener {
	 
	 public Invoice()
	 {
		 mView.addInvActionListener(this);
	 }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		new InvoiceController();
	}
	 
	 
}

	 class Exit implements ActionListener {
	 
	 public Exit()
	 {
		 mView.addExitActionListener(this);
	 }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		System.exit(0);
	}
	 
	 
}
	 class Info implements ActionListener {
		 
		public Info() {
			mView.addInfoActionListener(this);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			new ReadMe();
		}
		 
	 }
	 
	 public static void main (String [] args) 
	 {
	   	new MainController();
	    	
	 }

}
