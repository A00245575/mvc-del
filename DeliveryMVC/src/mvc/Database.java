package mvc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Database {
	
	public static Connection con = null;
	public static Statement stmt = null;
	public static Statement stmt2=null;
	public static ResultSet rs = null;
	
	public Database()
	{
		init_db();
	}
	public static void init_db()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url="jdbc:mysql://localhost:3306/NewsAgentDB?verifyServerCertificate=false&useSSL=true&jdbcCompliantTruncation=false";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}

}
