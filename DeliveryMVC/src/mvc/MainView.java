package mvc;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MainView extends JFrame {
	
	private JButton publications = new JButton("Publications");
	private JButton delivery = new JButton("Deliveries");
	private JButton customers = new JButton("Customers");
	private JButton staff = new JButton("Staff");
	private JButton subscriptions = new JButton("Subscriptions");
	private JButton invoice = new JButton("Invoices");
	private JButton exit = new JButton("Exit");
	private JButton readme = new JButton("Info");
	
	private Container contentPane2 = this.getContentPane();
	
	public MainView()
	{
		this.setSize(800, 600);
        this.setTitle("Newsagent Database System");
        this.setLayout(new GridLayout(0, 2));
	}
	
	public void init()
	{
		contentPane2.add(publications);
		contentPane2.add(customers);
		contentPane2.add(staff);
		contentPane2.add(subscriptions);
		contentPane2.add(delivery);
		contentPane2.add(invoice);
		contentPane2.add(readme);
		contentPane2.add(exit);
		this.setVisible(true);
	}
	
	public void addCustomersActionListener(ActionListener l)
	{
		customers.addActionListener(l);
	}
	
	public void addPublicationsActionListener(ActionListener l)
	{
		publications.addActionListener(l);
	}
	
	public void addInfoActionListener(ActionListener l)
	{
		readme.addActionListener(l);
	}
	
	public void addStaffActionListener(ActionListener l)
	{
		staff.addActionListener(l);
	}
	
	public void addSubActionListener(ActionListener l)
	{
		subscriptions.addActionListener(l);
	}
	
	public void addDelActionListener(ActionListener l)
	{
		delivery.addActionListener(l);
	}
	
	public void addInvActionListener(ActionListener l)
	{
		invoice.addActionListener(l);
	}
	
	public void addExitActionListener(ActionListener l)
	{
		exit.addActionListener(l);
	}
	

}
