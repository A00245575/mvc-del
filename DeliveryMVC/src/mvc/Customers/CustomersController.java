package mvc.Customers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import mvc.Database;
/*import mvc.Customers.NewCustomersController;
import mvc.Customers.CustomersModel;
import mvc.Customers.CustomersView;
import mvc.Customers.CustomersController.addCustomer;
import mvc.Customers.CustomersController.deleteCustomer;
import mvc.Customers.CustomersController.firstCustomer;
import mvc.Customers.CustomersController.lastCustomer;
import mvc.Customers.CustomersController.nextCustomer;
import mvc.Customers.CustomersController.previousCustomer;*/

public class CustomersController {

	private CustomersView sView = new CustomersView();
	private Database db = new Database();
	ArrayList<CustomersModel> sList = new ArrayList<CustomersModel>();
	private int count = 0;

	public 	CustomersController() {
		onload();
		sView.init();
		new firstCustomer();
		new previousCustomer();
		new lastCustomer();
		new nextCustomer();
		new addCustomer();
		new deleteCustomer();
	}

	public ArrayList<CustomersModel> onload() {

		
		String str = "select * from customers;";
		try {
			Database.rs = Database.stmt.executeQuery(str);
			while (Database.rs.next()) {

				int cus_id = Database.rs.getInt("cus_id");
				String first_name = Database.rs.getString("first_name");
				String last_name = Database.rs.getString("last_name");
				String address_line1 = Database.rs.getString("address_line1");
				String eircode = Database.rs.getString("eircode");
				//int sub_id = Database.rs.getInt("sub_id");
				//int pub_id = Database.rs.getInt("pub_id");
				//String title = Database.rs.getString("title");
				//double price = Database.rs.getDouble("price");
				//String type = Database.rs.getString("type");
				//String freq = Database.rs.getString("frequency");
				Date dob = Database.rs.getDate("dob");
				CustomersModel cus = new CustomersModel();
				cus.setCus_id(cus_id);
				cus.setFirst_name(first_name);
				cus.setLast_name(last_name);
				cus.setAdd_line1(address_line1);
				cus.setEircode(eircode);
				//cus.setSub_id(sub_id);
				//cus.setPub_id(pub_id);
				//cus.setTitle(title);
				//cus.setPrice(price);
				//cus.setType(type);
				//cus.setFrequency(freq);
				cus.setOrd_date(dob);
				sList.add(count, cus);
			}

		} catch (Exception e) {
			System.out.println("Error in loading Database");
		}
		return sList;
	}

	public void displayData() {

		sView.setcusID(Integer.toString(sList.get(count).getCus_id()));
		sView.setFirstName(sList.get(count).getFirst_name());
		sView.setLastName(sList.get(count).getLast_name());
		sView.setAddress1(sList.get(count).getAdd_line1());
		sView.setEircode(sList.get(count).getEircode());
		sView.setSubID(Integer.toString(sList.get(count).getSub_id()));
		sView.setTitles(sList.get(count).getTitle());
		sView.setType(sList.get(count).getType());
		sView.setPrice(Double.toString(sList.get(count).getPrice()));
		sView.setPubID(Integer.toString(sList.get(count).getPub_id()));
		sView.setFreq(sList.get(count).getFrequency());
		sView.setDob((sList.get(count).getOrd_date().toString()));

	}

	class firstCustomer implements ActionListener {

		public firstCustomer() {
			sView.addFirstActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = 0;
			displayData();
		}

	}

	class previousCustomer implements ActionListener {

		public previousCustomer() {
			sView.addPrevActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count > 0) {
				count--;
				System.out.println("Prev");
			}
			displayData();

		}

	}

	class lastCustomer implements ActionListener {

		public lastCustomer() {
			sView.addLastActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = sList.size() - 1;
			displayData();
		}

	}

	class nextCustomer implements ActionListener {

		public nextCustomer() {
			sView.addNextActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count < sList.size() - 1) {
				count++;
				System.out.println("Next");

			}
			displayData();
		}

	}

	class addCustomer implements ActionListener {

		public addCustomer() {
			sView.addAddActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			sView.dispose();
			new NewCustomersController();
		}

	}

	class deleteCustomer implements ActionListener {

		public deleteCustomer() {
			sView.addDeleteActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int cus_id = Integer.parseInt(sView.getCusID());
			String str = "Delete From customers where cus_id= " + cus_id + ";";
			try {
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(sView, "Sucessfully Deleted!!!", null, JOptionPane.PLAIN_MESSAGE);
				displayData();
			} catch (SQLException sqle) {
				System.out.println(sqle.getMessage());
				JOptionPane.showMessageDialog(sView, "Unsucessfully added!!!", null, JOptionPane.PLAIN_MESSAGE);

			}
		}
	}

}

