package mvc.Customers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import javax.swing.JOptionPane;

import mvc.Database;

public class NewCustomersController {
	
	private NewCustomersView sView = new NewCustomersView();
	//private CustomersView cView = new CustomersView();
    private Database db = new Database();

    public NewCustomersController()
    {
    	sView.init();
    	new addCustomer();
    	new returns();
    }
    
    class addCustomer implements ActionListener {
    	
    	public addCustomer()
    	{
    		sView.addInsertActionListener(this);
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int cus_id = Integer.parseInt(sView.getCusID());
			//int reg_id = Integer.parseInt(sView.getRegID());
			String first_name = sView.getFirstName();
			String last_name = sView.getLastName();
			String address_line1 = sView.getAddress1();
			String address_line2 = sView.getAddress2();
			String dob = sView.getDob();
			String eircode = sView.getEircode();
			
			String str = "INSERT INTO customers VALUES (null,'"+ first_name + "', '"+ last_name +"' ," +dob+" , '"+address_line1+"' ,'"+address_line2+"' , '"+eircode+"');";
			try
			{
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(sView, "Sucessfully added!!!", "New Subscription", JOptionPane.PLAIN_MESSAGE);

			}
			catch(SQLException sqle)
			{
				System.out.println(sqle.getMessage());
				JOptionPane.showMessageDialog(sView, "Unsucessfully added!!!", "New Subscription", JOptionPane.PLAIN_MESSAGE);

			}
		}
    }
    class returns implements ActionListener
	{
		public returns()
		{
			sView.addReturnActionListener(this);
			//test
		}

		@Override
		public void actionPerformed(ActionEvent l) {
			// TODO Auto-generated method stub
			sView.dispose();
			new CustomersController();
		}
		
	}

}
