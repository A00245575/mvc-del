package mvc.Customers;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.sql.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class CustomersView extends JFrame {
	
	private JLabel cusID = new JLabel("Customer ID:");
	
	private JLabel fName = new JLabel("First Name:");
	private JLabel lName = new JLabel("Last Name:");
	private JLabel address1 = new JLabel("Address Line 1:");
	private JLabel address2 = new JLabel("Address Line 2:");
	private JLabel eircode = new JLabel("Eircode:");
	private JLabel title = new JLabel("Title:");
	private JLabel type = new JLabel("Type:");
	private JLabel price = new JLabel("Price:");
	private JLabel subID = new JLabel("Subscription ID:");
	private JLabel pubID = new JLabel("Publication ID:");
	private JLabel frequency = new JLabel("Frequency:");
	private JLabel dob= new JLabel("Date:");
	private JLabel cusDetails = new JLabel("Customer Details");
	private JLabel pubDetails = new JLabel("Publication Details:");
	private JLabel empty1 = new JLabel("------");
	private JLabel empty2 = new JLabel("------");
	private JLabel empty3 = new JLabel("------");

	private JTextField cusID_data = new JTextField("");
	private JTextField fName_data = new JTextField("");
	private JTextField lName_data = new JTextField("");
	private JTextField address1_data = new JTextField("");
	private JTextField address2_data = new JTextField("");
	private JTextField eircode_data = new JTextField("");
	private JTextField title_data = new JTextField("");
	private JTextField type_data = new JTextField("");
	private JTextField price_data = new JTextField("");
	private JTextField subID_data = new JTextField("");
	private JTextField pubID_data = new JTextField("");
	private JTextField freq_data = new JTextField("");
	private JTextField dob_data = new JTextField("");
	private JTextField regID_data = new JTextField("");

	private JButton prev = new JButton("Previous");
	private JButton first = new JButton("First");
	private JButton last = new JButton("Last");
	private JButton next = new JButton("Next");
	private JButton add = new JButton("Add");
	private JButton delete = new JButton("Delete");

	public void addPrevActionListener(ActionListener l) {
		prev.addActionListener(l);
	}

	public void addFirstActionListener(ActionListener l) {
		first.addActionListener(l);
	}

	public void addLastActionListener(ActionListener l) {
		last.addActionListener(l);
	}

	public void addNextActionListener(ActionListener l) {
		next.addActionListener(l);
	}

	public void addAddActionListener(ActionListener l) {
		add.addActionListener(l);
	}

	public void addDeleteActionListener(ActionListener l) {
		delete.addActionListener(l);
	}

	private Container contentPane = this.getContentPane();

	public CustomersView() {
		this.setSize(800, 600);
		this.setTitle("Customer Records");
		this.setLayout(new GridLayout(0, 2));
	}

	public void init() {
		contentPane.add(cusDetails);
		contentPane.add(empty1);

		contentPane.add(cusID);
		contentPane.add(cusID_data);

		contentPane.add(fName);
		contentPane.add(fName_data);

		contentPane.add(lName);
		contentPane.add(lName_data);

		contentPane.add(address1);
		contentPane.add(address1_data);
		
		contentPane.add(address2);
		contentPane.add(address2_data);

		contentPane.add(eircode);
		contentPane.add(eircode_data);

		contentPane.add(dob);
		contentPane.add(dob_data);

		contentPane.add(prev);
		contentPane.add(next);

		contentPane.add(first);
		contentPane.add(last);

		contentPane.add(add);
		contentPane.add(delete);

		this.setVisible(true);
	}

	public String getDob() {
		return dob_data.getText();
	}

	public void setDob(String s) {
		dob_data.setText(s);
	}

	public String getCusID() {
		return cusID_data.getText();
	}

	public void setcusID(String s) {
		cusID_data.setText(s);
	}
	
	public String getRegID()
	{
		return regID_data.getText();
	}
	
	public void setRegID(String s)
	{
		regID_data.getText();
	}

	public String getFirstName() {
		return fName_data.getText();
	}

	public void setFirstName(String s) {
		fName_data.setText(s);
	}

	public String getLastName() {
		return lName_data.getText();
	}

	public void setLastName(String s) {
		lName_data.setText(s);
	}

	public String getAddress1() {
		return address1_data.getText();
	}

	public void setAddress1(String s) {
		address1_data.setText(s);
		;
	}
	
	public String getAddress2() {
		return address2_data.getText();
	}

	public void setAddress2(String s) {
		address2_data.setText(s);
		;
	}

	public String getEircode() {
		return eircode_data.getText();
	}

	public void setEircode(String s) {
		eircode_data.setText(s);
		;
	}

	public String getTitle() {
		return title_data.getText();
	}

	public void setTitles(String s) {
		title_data.setText(s);
		;
	}

	public String getTypes() {
		return type_data.getText();
	}

	public void setType(String s) {
		type_data.setText(s);
		;
	}

	public String getPrice() {
		return price_data.getText();
	}

	public void setPrice(String s) {
		price_data.setText(s);
	}

	public String getSubID() {
		return subID_data.getText();
	}

	public void setSubID(String s) {
		subID_data.setText(s);
	}

	public String getPubID() {
		return pubID_data.getText();
	}

	public void setPubID(String s) {
		pubID_data.setText(s);
	}

	public String getFreq() {
		return freq_data.getText();
	}

	public void setFreq(String s) {
		freq_data.setText(s);
	}

	
}
