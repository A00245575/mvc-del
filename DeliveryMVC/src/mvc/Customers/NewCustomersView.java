package mvc.Customers;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class NewCustomersView extends JFrame{

	private JLabel cusID = new JLabel("Customer ID:");
	/*private JLabel pubID = new JLabel("Publication ID:");
	private JLabel freq = new JLabel("Frequency:");*/
	private JLabel dob = new JLabel("Date(YYYY-MM-DD):");
	

	
	private JLabel fName = new JLabel("First Name:");
	private JLabel lName = new JLabel("Last Name:");
	private JLabel address1 = new JLabel("Address Line 1:");
	private JLabel address2 = new JLabel("Address Line 2:");
	private JLabel eircode = new JLabel("Eircode:");
	
	

	private JTextField cusID_data = new JTextField("");
	/*private JTextField pubID_data = new JTextField("");*/
	/*String [] freqs = {"Daily", "Weekly", "Monthly"};
	private JComboBox freq_data = new JComboBox(freqs);*/
	private JTextField ordDate_data = new JTextField("");
	
	private JTextField fName_data = new JTextField("");
	private JTextField lName_data = new JTextField("");
	private JTextField address1_data = new JTextField("");
	private JTextField address2_data = new JTextField("");
	private JTextField eircode_data = new JTextField("");
	private JTextField regID_data = new JTextField("");
	private JTextField dob_data = new JTextField("");
	

	private JButton insert = new JButton("Insert");
	private JButton ret = new JButton("Return");

	private Container contentPane = this.getContentPane();

	public void addInsertActionListener(ActionListener l) {
		insert.addActionListener(l);
	}

	public void addReturnActionListener(ActionListener l) {
		ret.addActionListener(l);
	}

	public NewCustomersView() {
		this.setSize(800, 600);
		this.setTitle("Add new Customer");
		this.setLayout(new GridLayout(0, 2));
	}

	public void init() {
		contentPane.add(cusID);
		contentPane.add(cusID_data);
		contentPane.add(fName);
		contentPane.add(fName_data);
		contentPane.add(lName);
		contentPane.add(lName_data);
		contentPane.add(address1);
		contentPane.add(address1_data);
		contentPane.add(address2);
		contentPane.add(address2_data);
		contentPane.add(eircode);
		contentPane.add(eircode_data);
		/*contentPane.add(pubID);
		contentPane.add(pubID_data);
		contentPane.add(freq);
		contentPane.add(freq_data);*/
		contentPane.add(dob);
		contentPane.add(dob_data);
		contentPane.add(insert);
		contentPane.add(ret);
		this.setVisible(true);
	}

	public String getDob() {
		return dob_data.getText();
	}

	public void setDob(String s) {
		dob_data.setText(s);
	}
	
	

	public String getCusID() {
		return cusID_data.getText();
	}

	public void setcusID(String s) {
		cusID_data.setText(s);
	}

	/*public String getPubID() {
		return pubID_data.getText();
	}

	public void setPubID(String s) {
		pubID_data.setText(s);
	}

	public String getFreq_data() {
		return freq_data.getSelectedItem().toString();
	}

	public void setFreq_data(String s) {
		freq_data.toString();
	}*/
	
	public String getRegID()
	{
		return regID_data.getText();
	}
	
	public void setRegID(String s)
	{
		regID_data.getText();
	}
	
	public String getFirstName() {
		return fName_data.getText();
	}

	public void setFirstName(String s) {
		fName_data.setText(s);
	}

	public String getLastName() {
		return lName_data.getText();
	}

	public void setLastName(String s) {
		lName_data.setText(s);
	}

	public String getAddress1() {
		return address1_data.getText();
	}

	public void setAddress1(String s) {
		address1_data.setText(s);
		;
	}
	
	public String getAddress2() {
		return address2_data.getText();
	}

	public void setAddress2(String s) {
		address2_data.setText(s);
		;
	}

	public String getEircode() {
		return eircode_data.getText();
	}

	public void setEircode(String s) {
		eircode_data.setText(s);
		;
	}

}
