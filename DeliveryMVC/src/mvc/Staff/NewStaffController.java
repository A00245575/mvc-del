package mvc.Staff;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import mvc.Database;

public class NewStaffController {
	
	private NewStaffView sView = new NewStaffView();
    private Database db = new Database();

	
	public NewStaffController()
	{
		sView.init();
		new addStaff();
		new returns();
	}
	
	class addStaff implements ActionListener {
    	
    	public addStaff()
    	{
    		sView.addInsertActionListener(this);
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int driver_id = Integer.parseInt(sView.getDriverID());
			String first_name = sView.getFirstName();
			String last_name = sView.getLastName();
			String PhoneNo = sView.getPhoneNo();
			String dob = sView.getDob();
			String str = "INSERT INTO Delivery VALUES (null,'"  + "',"+ driver_id + ", '"+ first_name +"' ,'" + last_name +", '"+ PhoneNo +"' , '"+ dob +"');";
			
			try
			{
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(sView, "Sucessfully added!!!", "New Staff", JOptionPane.PLAIN_MESSAGE);

				

			}
			catch(SQLException sqle)
			{
				System.out.println(sqle.getMessage());
				JOptionPane.showMessageDialog(sView, "Unsucessfully added!!!", "New Staff", JOptionPane.PLAIN_MESSAGE);
			}
		}
    	
    }
	class returns implements ActionListener
	{
		public returns()
		{
			sView.addReturnActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent l) {
			// TODO Auto-generated method stub
			sView.dispose();
			new StaffController();
		}
		
	}
}