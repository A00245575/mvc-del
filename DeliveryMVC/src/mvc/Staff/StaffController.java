package mvc.Staff;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import mvc.Database;
import mvc.Customers.CustomersModel;


public class StaffController {

	private StaffView sView = new StaffView();
	private Database db = new Database();
	ArrayList<StaffModel> sList = new ArrayList<StaffModel>();
	private int count;

	public StaffController() {

		onload();
		sView.init();
		new firstStaff();
		new previousStaff();
		new lastStaff();
		new nextStaff();
		new addStaff();
		new deleteStaff();

	}

	public ArrayList<StaffModel> onload() {

		String str = "select * from driver;";
		try {
			Database.rs = Database.stmt.executeQuery(str);
			while (Database.rs.next()) {

				int driver_id = Database.rs.getInt("driver_id");				
				String first_name = Database.rs.getString("first_name");
				String last_name = Database.rs.getString("last_name");
				String PhoneNo = Database.rs.getString("Phone_Number");
				String dob = Database.rs.getString("Date_Of_Birth");
				
				StaffModel s = new StaffModel();
				s.setDriver_id(driver_id);				
				s.setFirst_name(first_name);
				s.setLast_name(last_name);
				s.setPhoneNo(PhoneNo);
				s.setDob(dob);				
								
				sList.add(count,s);
				
			}
			displayData();

		} catch (Exception e) {
			System.out.println("Error in loading Database");
		}
		return sList;
	}

	public void displayData() {
		
		sView.setDriverID(Integer.toString(sList.get(count).getDriver_id()));
		sView.setFirstName(sList.get(count).getFirst_name());
		sView.setLastName(sList.get(count).getLast_name());
		sView.setPhoneNo(sList.get(count).getPhoneNo());
		sView.setDob(sList.get(count).getDob());
		
	}

	class firstStaff implements ActionListener {

		public firstStaff() {
			sView.addFirstActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = 0;
			onload();
			displayData();
		}

	}

	class previousStaff implements ActionListener {

		public previousStaff() {
			sView.addPrevActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count > 0) {
				count--;
			}

			displayData();
		}

	}

	class lastStaff implements ActionListener {

		public lastStaff() {
			sView.addLastActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = sList.size() - 1;
		}

	}

	class nextStaff implements ActionListener {

		public nextStaff() {
			sView.addNextActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count < sList.size() - 1) {
				count++;
			}
			displayData();
		}

	}

	class addStaff implements ActionListener {

		public addStaff() {

			sView.addAddActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			sView.dispose();
			new NewStaffController();
		}

	}

	class deleteStaff implements ActionListener {

		public deleteStaff() {
			sView.addDeleteActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int driver_id = Integer.parseInt(sView.getDriverID());
			String str = "Delete From Staff where driver_id= " + driver_id + ";";
			try {
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(sView, "Sucessfully Deleted!!!", null, JOptionPane.PLAIN_MESSAGE);
				displayData();
			} catch (SQLException sqle) {
				System.out.println();
				JOptionPane.showMessageDialog(sView, "Unsucessfully added!!!" + sqle.getMessage(), null,
						JOptionPane.PLAIN_MESSAGE);

			}

		}

	}

}