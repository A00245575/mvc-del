package mvc.Staff;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class NewStaffView extends JFrame {
	
	private JLabel driverID = new JLabel("Staff ID:");
	private JLabel fName = new JLabel("First Name:");
	private JLabel lName = new JLabel("Last Name:");
	private JLabel phoneNo = new JLabel("Phone Number:");
	private JLabel dob = new JLabel("Date Of Birth:");	
	
	
	private JTextField driverID_data = new JTextField("");
	private JTextField fName_data = new JTextField("");
	private JTextField lName_data = new JTextField("");
	private JTextField phoneNo_data = new JTextField("");
	private JTextField dob_data = new JTextField("");
	
	private JButton insert = new JButton("Insert");
	private JButton ret = new JButton("Return");
	
	private Container contentPane = this.getContentPane();
	
	public void addInsertActionListener (ActionListener l)
    {
        insert.addActionListener(l);
    }
 
	public void addReturnActionListener (ActionListener l)
    {
        ret.addActionListener(l);
    }
 
	public NewStaffView()
	{
		this.setSize(800, 600);
        this.setTitle("Add new Staff");
        this.setLayout(new GridLayout(0, 2));
	}
	
	public void init()
	{
		contentPane.add(driverID);
		contentPane.add(driverID_data);
		contentPane.add(fName);
		contentPane.add(fName_data);
		
		contentPane.add(lName);
		contentPane.add(lName_data);
		
		contentPane.add(phoneNo);
		contentPane.add(phoneNo_data);
		
		contentPane.add(dob);
		contentPane.add(dob_data);
		
		
		contentPane.add(insert);
		contentPane.add(ret);
		this.setVisible(true);
	}
 

	public String getDriverID()
	{
		return driverID_data.getText();
	}
	public void setDriverID(String s)
	{
		driverID_data.setText(s);	
	}
	
	public String getFirstName()
	{
		return fName_data.getText();
	}
	public void setFirstName(String s)
	{
		fName_data.setText(s);	
	}
	
	public String getLastName()
	{
		return lName_data.getText();
	}
	public void setLastName(String s)
	{
		lName_data.setText(s);	
	}
	

	public String getPhoneNo() {
		return phoneNo_data.getText();
	}

	public void setPhoneNo(String s) {
		phoneNo_data.setText(s);;
	}

	public String getDob() {
		return dob_data.getText();
	}

	public void setDob(String s) {
		dob_data.setText(s);;
	}

}