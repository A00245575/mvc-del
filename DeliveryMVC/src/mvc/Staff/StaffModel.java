package mvc.Staff;

import java.sql.Date;

public class StaffModel {

	private  int driver_id;	
	private String first_name;
	private String last_name;
	private String PhoneNo;
	private String dob;
	

	
	
	
	//setters
	public void setDriver_id(int s)
    {
		driver_id = s;
    }
   
    
    public void setFirst_name(String s)
    {
    	first_name = s;
    }
    
    public void setLast_name(String s)
    {
    	last_name = s;
    }
    
    public void setPhoneNo(String s)
    {
    	PhoneNo = s;
    }
    
    public void setDob(String s)
    {
    	dob = s;
    }
    
    
    
    
    
    //getters
    public int getDriver_id()
    {
    	return driver_id;
    }
    
   
    
    public String getFirst_name()
    {
    	return first_name;
    }
    
    public String getLast_name()
    {
    	return last_name;
    }
    
    public String getPhoneNo()
    {
    	return PhoneNo;
    }
    
    public String getDob()
    {
    	return dob;
    }
    
   
}