package mvc.Staff;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class StaffView extends JFrame {
	
	//init Jlabels
	private JLabel driverID = new JLabel("Staff ID:");
	private JLabel fName = new JLabel("First Name:");
	private JLabel lName = new JLabel("Last Name:");
	private JLabel phoneNo = new JLabel("Phone Number:");
	private JLabel dob = new JLabel("Date Of Birth:");	
	private JLabel staffDetails = new JLabel("Staff Details");	
	private JLabel empty1 = new JLabel("------");
	
	
	
	//init JTextFields
	private JTextField driverID_data = new JTextField("");
	private JTextField fName_data = new JTextField("");
	private JTextField lName_data = new JTextField("");
	private JTextField phoneNo_data = new JTextField("");
	private JTextField dob_data = new JTextField("");
	
	
	//init JButtons
	private JButton prev = new JButton("Previous");
	private JButton first = new JButton("First");
	private JButton last = new JButton("Last");
	private JButton next = new JButton("Next");
	private JButton add = new JButton("Add");
	private JButton delete = new JButton("Delete");
	
	 public void addPrevActionListener (ActionListener l)
	    {
	        prev.addActionListener(l);
	    }
	 
	 public void addFirstActionListener (ActionListener l)
	    {
	        first.addActionListener(l);
	    }
	 
	 public void addLastActionListener (ActionListener l)
	    {
	        last.addActionListener(l);
	    }
	 
	 public void addNextActionListener (ActionListener l)
	    {
	        next.addActionListener(l);
	    }
	 
	 public void addAddActionListener (ActionListener l)
	    {
	        add.addActionListener(l);
	    }
	 
	 public void addDeleteActionListener (ActionListener l)
	    {
	        delete.addActionListener(l);
	    }
	
	//init JCheckBox
	private JCheckBox delivered = new JCheckBox("Delivered"); 
	
	private Container contentPane = this.getContentPane();
	
	public StaffView()
	{
		this.setSize(800, 600);
        this.setTitle("Staff Records");
        this.setLayout(new GridLayout(0, 2));
	}
	
	public void init()
	{
		contentPane.add(staffDetails);
		contentPane.add(empty1);
		
		contentPane.add(driverID);
		contentPane.add(driverID_data);
		
		contentPane.add(fName);
		contentPane.add(fName_data);
		
		contentPane.add(lName);
		contentPane.add(lName_data);
		
		contentPane.add(phoneNo);
		contentPane.add(phoneNo_data);
		
		contentPane.add(dob);
		contentPane.add(dob_data);
		
		
		
		contentPane.add(prev);
		contentPane.add(next);
		
		contentPane.add(first);
		contentPane.add(last);

		contentPane.add(add);
		contentPane.add(delete);
		
		this.setVisible(true);
		
	}
	
	
	
	public String getDriverID()
	{
		return driverID_data.getText();
	}
	public void setDriverID(String s)
	{
		driverID_data.setText(s);	
	}
	
	public String getFirstName()
	{
		return fName_data.getText();
	}
	public void setFirstName(String s)
	{
		fName_data.setText(s);	
	}
	
	public String getLastName()
	{
		return lName_data.getText();
	}
	public void setLastName(String s)
	{
		lName_data.setText(s);	
	}
	

	public String getPhoneNo() {
		return phoneNo_data.getText();
	}

	public void setPhoneNo(String s) {
		phoneNo_data.setText(s);;
	}

	public String getDob() {
		return dob_data.getText();
	}

	public void setDob(String s) {
		dob_data.setText(s);;
	}

	
	
	




}