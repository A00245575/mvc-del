package mvc.Invoice;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import mvc.Database;


public class NewInvoiceController {
	
	private NewInvoiceView iView = new NewInvoiceView();
	private Database db = new Database();
	
	public NewInvoiceController()
	{
		iView.init();
		new addInvoice();
		new returns();
	}
	
	class addInvoice implements ActionListener {

		public addInvoice() {
			iView.addInsertActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			//newDelivery();
			int cus_id = Integer.parseInt(iView.getCusID());
			java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
			String str = "INSERT INTO Invoice VALUES (null," + cus_id + ",'" + sqlDate + "');";
			try{
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(iView, "Sucessfully added!!!", "Generate Invoice", JOptionPane.PLAIN_MESSAGE);
			}
			catch(SQLException e1)
			{
				JOptionPane.showMessageDialog(iView, "Unsucessfully added!!!", "Generate Invoice", JOptionPane.PLAIN_MESSAGE);
			}
		}

	}

	class returns implements ActionListener {
		public returns() {
			iView.addReturnActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent l) {
			// TODO Auto-generated method stub
			iView.dispose();
			new InvoiceController();
		}

	}

}
