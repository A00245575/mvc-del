package mvc.Invoice;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class InvoiceView extends JFrame{
	
	private JLabel cusDetails = new JLabel("Customer Details");
	private JLabel empty1 = new JLabel("------");
	private JLabel invDetails = new JLabel("Invoice Details");
	private JLabel empty2 = new JLabel("------");
	private JLabel cusID = new JLabel("Customer ID:");
	private JLabel fName = new JLabel("First Name:");
	private JLabel lName = new JLabel("Last Name:");
	private JLabel address1 = new JLabel("Address Line 1:");
	private JLabel eircode = new JLabel("Eircode:");
	private JLabel region = new JLabel("Region:");
	
	private JLabel Inv_id = new JLabel("Invoice ID:");
	private JLabel noOfPub = new JLabel("Total Publications:");
	private JLabel totalPrice = new JLabel("Total Price: ");
	private JLabel inv_date = new JLabel("Date:");
	
	private JTextField cusID_data = new JTextField("");
	private JTextField fName_data = new JTextField("");
	private JTextField lName_data = new JTextField("");
	private JTextField address1_data = new JTextField("");
	private JTextField eircode_data = new JTextField("");
	private JTextField region_data = new JTextField("") ;
	
	private JTextField invID_data = new JTextField("") ;
	private JTextField noOfPub_data = new JTextField("") ;
	private JTextField total_data = new JTextField("") ;
	private JTextField invDate_data = new JTextField("");
	
	private JButton prev = new JButton("Previous");
	private JButton first = new JButton("First");
	private JButton last = new JButton("Last");
	private JButton next = new JButton("Next");
	private JButton add = new JButton("Add");
	private JButton delete = new JButton("Delete");
	
	private Container contentPane = this.getContentPane();

	public void addPrevActionListener(ActionListener l) {
		prev.addActionListener(l);
	}

	public void addFirstActionListener(ActionListener l) {
		first.addActionListener(l);
	}

	public void addLastActionListener(ActionListener l) {
		last.addActionListener(l);
	}

	public void addNextActionListener(ActionListener l) {
		next.addActionListener(l);
	}

	public void addAddActionListener(ActionListener l) {
		add.addActionListener(l);
	}

	public void addDeleteActionListener(ActionListener l) {
		delete.addActionListener(l);
	}

	

	public InvoiceView() {
		this.setSize(800, 600);
		this.setTitle("Invoice Records");
		this.setLayout(new GridLayout(0, 2));
	}
	
	public void init()
	{	
		contentPane.add(cusDetails);
		contentPane.add(empty1);
		
		contentPane.add(cusID);
		contentPane.add(cusID_data);
		cusID_data.setEditable(false);

		contentPane.add(fName);
		contentPane.add(fName_data);
		fName_data.setEditable(false);

		contentPane.add(lName);
		contentPane.add(lName_data);
		lName_data.setEditable(false);

		contentPane.add(address1);
		contentPane.add(address1_data);
		address1_data.setEditable(false);

		contentPane.add(eircode);
		contentPane.add(eircode_data);
		eircode_data.setEditable(false);
		
		contentPane.add(region);
		contentPane.add(region_data);
		region_data.setEditable(false);
		
		contentPane.add(invDetails);
		contentPane.add(empty2);
		
		contentPane.add(Inv_id);
		contentPane.add(invID_data);
		invID_data.setEditable(false);
		
		contentPane.add(noOfPub);
		contentPane.add(noOfPub_data);
		noOfPub_data.setEditable(false);
		
		contentPane.add(totalPrice);
		contentPane.add(total_data);
		total_data.setEditable(false);
		
		contentPane.add(inv_date);
		contentPane.add(invDate_data);
		invDate_data.setEditable(false);
		
		contentPane.add(prev);
		contentPane.add(next);
		
		contentPane.add(first);
		contentPane.add(last);

		contentPane.add(add);
		contentPane.add(delete);
		
		this.setVisible(true);
		
	}
	
	public String getInvDate() {
		return invDate_data.getText();
	}

	public void setInvDate(String s) {
		invDate_data.setText(s);
	}
	
	public String getCusID() {
		return cusID_data.getText();
	}

	public void setcusID(String s) {
		cusID_data.setText(s);
	}

	public String getFirstName() {
		return fName_data.getText();
	}

	public void setFirstName(String s) {
		fName_data.setText(s);
	}

	public String getLastName() {
		return lName_data.getText();
	}

	public void setLastName(String s) {
		lName_data.setText(s);
	}

	public String getAddress1() {
		return address1_data.getText();
	}

	public void setAddress1(String s) {
		address1_data.setText(s);
		;
	}

	public String getEircode() {
		return eircode_data.getText();
	}

	public void setEircode(String s) {
		eircode_data.setText(s);
		;
	}
	
	public String getRegion() {
		return region_data.getText();
	}

	public void setRegion(String s) {
		region_data.setText(s);
		;
	}
	
	public String getInvID() {
		return invID_data.getText();
	}

	public void setInvoice(String s) {
		invID_data.setText(s);
		;
	}
	
	public String getNoOfPub() {
		return noOfPub_data.getText();
	}

	public void setNoOfPub(String s) {
		noOfPub_data.setText(s);
		;
	}
	
	public String getTotal() {
		return total_data.getText();
	}

	public void setTotal(String s) {
		total_data.setText(s);
		;
	}

}
