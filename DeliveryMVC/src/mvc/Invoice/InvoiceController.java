package mvc.Invoice;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import mvc.Database;

public class InvoiceController {
	
	private InvoiceView iView = new InvoiceView();
	private Database db = new Database();
	ArrayList<InvoiceModel> iList = new ArrayList<InvoiceModel>();
	private int count = 0;
	
	public InvoiceController()
	{
		onload();
		displayData();
		iView.init();
		new previousInvoice();
		new firstInvoice();
		new nextInvoice();
		new lastInvoice();
		new addInvoice();
		new deleteInvoice();
	}
	
	public void onload()
	{
		String str = "select Customers.cus_id, Customers.first_name, Customers.last_name, Customers.address_line1, Customers.eircode, Region.description, Invoice.inv_id, Invoice.inv_date from Customers, Region, Publication, Invoice, Subscribes where Customers.reg_id = Region.reg_id and Publication.pub_id = Subscribes.pub_id and Invoice.cus_id = Customers.cus_id and Customers.cus_id = Subscribes.cus_id;";
		
		try {
			Database.rs = Database.stmt.executeQuery(str);
			while (Database.rs.next()) {

				int cus_id = Database.rs.getInt("cus_id");
				String first_name = Database.rs.getString("first_name");
				String last_name = Database.rs.getString("last_name");
				String address_line1 = Database.rs.getString("address_line1");
				String eircode = Database.rs.getString("eircode");
				String region = Database.rs.getString("description");
				int inv_id = Database.rs.getInt("inv_id");
				//int noOfPubs = countPub(cus_id);
				//double price = Database.rs.getDouble("total");
				Date invDate = Database.rs.getDate("inv_date");
				InvoiceModel inv = new InvoiceModel();
				inv.setCus_id(cus_id);
				inv.setFirst_name(first_name);
				inv.setLast_name(last_name);
				inv.setAdd_line1(address_line1);
				inv.setEircode(eircode);
				inv.setRegion(region);
				inv.setInv_id(inv_id);
				inv.setNoOfPubs(countPub(cus_id));
				inv.setTotal(totalPrice(cus_id));
				inv.setInvDate(invDate);
				iList.add(inv);
				
				
			}
		}
			catch (Exception e) {
				System.out.println("Error in loading Database");
			}
	}
	
	public int countPub(int p)
	{
		int total = 0;
		String str = "Select sub_id, frequency from Subscribes, publication where subscribes.pub_id  = publication.pub_id and cus_id = " + p +";";
		try {
			Database.rs = Database.stmt.executeQuery(str);
			int pubs = 0;
			while (Database.rs.next()) {
				total = Database.rs.getInt("sub_id");
				String freq = Database.rs.getString("frequency");
				if(freq.equals("Daily"))
				{
					pubs = pubs + 30;
				}
				else if(freq.equals("Weekly"))
				{
					pubs = pubs + 4;
				}
				else if(freq.equals("Monthly"))
				{
					pubs = pubs + 1;
				}
			}
			return pubs;
		}
		catch(Exception e)
		{
			return 0;
		}	
	}
	
	public double totalPrice(int p)
	{
		double total = 0.0;
		double price = 0.0;
		String str = "Select price, frequency from Publication, Subscribes, Customers where publication.pub_id = subscribes.sub_id and customers.cus_id ="+ p +";";
		try {
			Database.rs = Database.stmt.executeQuery(str);
			while (Database.rs.next()) {
				total = Database.rs.getDouble("price");
				String freq = Database.rs.getString("frequency");
				if(freq.equals("Daily"))
				{
					total *= 30;
					price += total;
				}
				else if(freq.equals("Weekly"))
				{
					total *= 4;
					price += total;
				}
				else if(freq.equals("Monthly"))
				{
					total++;
					price += total;
				}
			}
			return round(price, 2);
		}
		catch(Exception e)
		{
			return 0;
		}	
		
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
	public void displayData()
	{
		iView.setcusID(Integer.toString(iList.get(count).getCus_id()));
		iView.setFirstName(iList.get(count).getFirst_name());
		iView.setLastName(iList.get(count).getLast_name());
		iView.setAddress1(iList.get(count).getAdd_line1());
		iView.setEircode(iList.get(count).getEircode());
		iView.setRegion(iList.get(count).getRegion());
		iView.setInvoice(Integer.toString(iList.get(count).getInv_id()));
		iView.setNoOfPub(Integer.toString(iList.get(count).getNoOfPubs()));
		iView.setTotal("�" + Double.toString(iList.get(count).getTotal()));
		iView.setInvDate(iList.get(count).getInvDate().toString());
	}
	
	class firstInvoice implements ActionListener {

		public firstInvoice() {
			iView.addFirstActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = 0;
			onload();
			displayData();
		}

	}

	class previousInvoice implements ActionListener {

		public previousInvoice() {
			iView.addPrevActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count > 0) {
				count--;
			}

			displayData();
		}

	}

	class lastInvoice implements ActionListener {

		public lastInvoice() {
			iView.addLastActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = iList.size() - 1;
		}

	}

	class nextInvoice implements ActionListener {

		public nextInvoice() {
			iView.addNextActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count < iList.size() - 1) {
				count++;
			}
			displayData();
		}

	}

	class addInvoice implements ActionListener {

		public addInvoice() {

			iView.addAddActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			iView.dispose();
			new NewInvoiceController();
		}

	}

	class deleteInvoice implements ActionListener {

		public deleteInvoice() {
			iView.addDeleteActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int inv_id = Integer.parseInt(iView.getInvID());
			String str = "Delete From Invoice where inv_id= " + inv_id + ";";
			try {
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(iView, "Sucessfully Deleted!!!", null, JOptionPane.PLAIN_MESSAGE);
				displayData();
			} catch (SQLException sqle) {
				System.out.println();
				JOptionPane.showMessageDialog(iView, "Unsucessfully Deleted!!!" + sqle.getMessage(), null,
						JOptionPane.PLAIN_MESSAGE);

			}

		}

	}

}
