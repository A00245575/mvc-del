package mvc;

import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ReadMe extends JFrame {
	String s = "Group Project 2018" + "\nSoftware Design Year 2" + "\n James Kidd - A00245575" + "\n Mark Hession - A00239393" + "\n Ross Hayden - A00000000" + "\n Cage Awor - A00000000" + "\n Gary Daly - A00000000";
	private JTextArea text = new JTextArea(s);
	
private Container contentPane = this.getContentPane();
	
	public ReadMe()
	{
		this.setSize(800, 600);
        this.setTitle("Info");
        this.setLayout(new GridLayout(0, 1));
        init();
	}
	
	public void init() {
		
		contentPane.add(text);
		this.setVisible(true);
		
	}

}
