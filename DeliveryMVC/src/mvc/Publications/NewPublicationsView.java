package mvc.Publications;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class NewPublicationsView extends JFrame {
	
	private JLabel pubID = new JLabel("Publication ID:");
	private JLabel title = new JLabel("Title:");
	private JLabel price = new JLabel("Price:");
	private JLabel type = new JLabel("Type:");
	private JLabel stock = new JLabel("Stock:");
	private JLabel frequency = new JLabel("Frequency:");
	private JTextField pubID_data = new JTextField("");
	private JTextField title_data = new JTextField("");
	private JTextField price_data = new JTextField("");
	private JTextField type_data = new JTextField("");
	private JTextField stock_data = new JTextField("");
	private JTextField frequency_data = new JTextField("");

	private JButton insert = new JButton("Insert");
	private JButton ret = new JButton("Return");

	private Container contentPane = this.getContentPane();
	
	public void addInsertActionListener(ActionListener l) {
		insert.addActionListener(l);
	}

	public void addReturnActionListener(ActionListener l) {
		ret.addActionListener(l);
	}

	public NewPublicationsView() {
		this.setSize(800, 600);
		this.setTitle("Add new Publication");
		this.setLayout(new GridLayout(0, 2));
	}
	
	public void init() {
		//contentPane.add(pubID);
		//contentPane.add(pubID_data);
		contentPane.add(title);
		contentPane.add(title_data);
		contentPane.add(price);
		contentPane.add(price_data);
		contentPane.add(type);
		contentPane.add(type_data);
		contentPane.add(stock);
		contentPane.add(stock_data);
		contentPane.add(frequency);
		contentPane.add(frequency_data);
		contentPane.add(insert);
		contentPane.add(ret);
		this.setVisible(true);
	}
	

	public String getPubID() {
		return pubID_data.getText();
	}

	public void setPubID(String s) {
		pubID_data.setText(s);
	}

	public String getTitles() {
		return title_data.getText();
	}
	
	public void setTitles(String s) {
		title_data.setText(s);
	}
	
	public String getPrice() {
		return price_data.getText();
	}
	
	public void setPrice(String s) {
		price_data.setText(s);
	}
	
	public String getTypes() {
		return type_data.getText();
	}
	
	public void setType(String s) {
		type_data.setText(s);
	}
	
	public String getStock() {
		return stock_data.getText();
	}
	
	public void setStock(String s) {
		stock_data.setText(s);
	}
	
	public String getFrequency() {
		return frequency_data.getText();
	}
	
	public void setFrequency(String s) {
		frequency_data.setText(s);
	}




}
