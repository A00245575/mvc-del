package mvc.Publications;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import javax.swing.JOptionPane;
import mvc.Database;

public class NewPublicationsController {
	
	private NewPublicationsView pView = new NewPublicationsView();
    private Database db = new Database();

    public NewPublicationsController()
    {
    	pView.init();
    	new addPublication();
    	new returns();
    }
    
    class addPublication implements ActionListener {
    	
    	public addPublication()
    	{
    		pView.addInsertActionListener(this);
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int stock = Integer.parseInt(pView.getStock());
			//int pub_id = Integer.parseInt(pView.getPubID());
			double price = Double.parseDouble(pView.getPrice());
			String title = pView.getTitles();
			String type = pView.getTypes();
			String frequency = pView.getFrequency();
			
			String str = "INSERT INTO publication VALUES (null,'"+ title +"' ," + price +", '"+ type +"' , '"+ frequency +"', "+ stock +");";
			try
			{
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(pView, "Sucessfully added!!!", "New Publication", JOptionPane.PLAIN_MESSAGE);

			}
			catch(SQLException sqle)
			{
				System.out.println(sqle.getMessage());
				JOptionPane.showMessageDialog(pView, "Unsucessfully added!!!", "New Publication", JOptionPane.PLAIN_MESSAGE);

			}
		}
    }
    
    class returns implements ActionListener
	{
		public returns()
		{
			pView.addReturnActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent l) {
			// TODO Auto-generated method stub
			pView.dispose();
			new PublicationsController();
		}
		
	}

}
