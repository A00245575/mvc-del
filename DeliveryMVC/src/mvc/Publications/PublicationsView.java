package mvc.Publications;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.sql.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PublicationsView extends JFrame {

	private JLabel title = new JLabel("Title:");
	private JLabel type = new JLabel("Type:");
	private JLabel price = new JLabel("Price:");
	private JLabel stock = new JLabel("Stock:");
	private JLabel frequency = new JLabel("Frequency:");
	private JLabel pubID = new JLabel("Publication ID:");
	private JLabel pubDetails = new JLabel("Publication Details:");
	private JLabel empty1 = new JLabel("------");
	private JLabel empty2 = new JLabel("------");
	private JLabel empty3 = new JLabel("------");

	private JTextField title_data = new JTextField("");
	private JTextField type_data = new JTextField("");
	private JTextField price_data = new JTextField("");
	private JTextField stock_data = new JTextField("");
	private JTextField pubID_data = new JTextField("");
	private JTextField frequency_data = new JTextField("");


	private JButton prev = new JButton("Previous");
	private JButton first = new JButton("First");
	private JButton last = new JButton("Last");
	private JButton next = new JButton("Next");
	private JButton add = new JButton("Add");
	private JButton delete = new JButton("Delete");
	
	public void addPrevActionListener(ActionListener l) {
		prev.addActionListener(l);
	}

	public void addFirstActionListener(ActionListener l) {
		first.addActionListener(l);
	}

	public void addLastActionListener(ActionListener l) {
		last.addActionListener(l);
	}

	public void addNextActionListener(ActionListener l) {
		next.addActionListener(l);
	}

	public void addAddActionListener(ActionListener l) {
		add.addActionListener(l);
	}

	public void addDeleteActionListener(ActionListener l) {
		delete.addActionListener(l);
	}

	private Container contentPane = this.getContentPane();

	public PublicationsView() {
		this.setSize(800, 600);
		this.setTitle("Delivery Records");
		this.setLayout(new GridLayout(0, 2));
	}

	public void init() {

		contentPane.add(pubDetails);
		contentPane.add(empty2);

		contentPane.add(pubID);
		contentPane.add(pubID_data);

		contentPane.add(title);
		contentPane.add(title_data);

		contentPane.add(type);
		contentPane.add(type_data);

		contentPane.add(price);
		contentPane.add(price_data);

		contentPane.add(stock);
		contentPane.add(stock_data);
		
		contentPane.add(frequency);
		contentPane.add(frequency_data);

		contentPane.add(prev);
		contentPane.add(next);

		contentPane.add(first);
		contentPane.add(last);

		contentPane.add(add);
		contentPane.add(delete);

		this.setVisible(true);
	}
	
	public String getTitle() {
		return title_data.getText();
	}

	public void setTitles(String s) {
		title_data.setText(s);
		;
	}

	public String getTypes() {
		return type_data.getText();
	}

	public void setType(String s) {
		type_data.setText(s);
		;
	}

	public String getPrice() {
		return price_data.getText();
	}

	public void setPrice(String s) {
		price_data.setText(s);
	}

	public String getStock() {
		return stock_data.getText();
	}

	public void setStock(String s) {
		stock_data.setText(s);
	}
	
	public String getFrequency() {
		return frequency_data.getText();
	}

	public void setFrequency(String s) {
		frequency_data.setText(s);
	}

	public String getPubID() {
		return pubID_data.getText();
	}

	public void setPubID(String s) {
		pubID_data.setText(s);
	}

}
