package mvc.Publications;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import mvc.Database;
import mvc.Publications.PublicationsModel;
import mvc.Publications.PublicationsView;
import mvc.Publications.PublicationsController.addPublication;
import mvc.Publications.PublicationsController.deletePublication;
import mvc.Publications.PublicationsController.firstPublication;
import mvc.Publications.PublicationsController.lastPublication;
import mvc.Publications.PublicationsController.nextPublication;
import mvc.Publications.PublicationsController.previousPublication;
import mvc.Subscription.NewSubscriptionController;

public class PublicationsController {
	
	private PublicationsView pView = new PublicationsView();
	private Database db = new Database();
	ArrayList<PublicationsModel> pList = new ArrayList<PublicationsModel>();
	private int count = 0;
	
	public PublicationsController() {
		onload();
		pView.init();
		new firstPublication();
		new previousPublication();
		new lastPublication();
		new nextPublication();
		new addPublication();
		new deletePublication();
	}
	
	public ArrayList<PublicationsModel> onload() {

		
		String str = "select * from publication;";
		try {
			Database.rs = Database.stmt.executeQuery(str);
			while (Database.rs.next()) {

				int pub_id = Database.rs.getInt("pub_id");
				String title = Database.rs.getString("title");
				double price = Database.rs.getDouble("price");
				String type = Database.rs.getString("type");
				int stock = Database.rs.getInt("stock"); 
				String frequency = Database.rs.getString("frequency");
				PublicationsModel sub = new PublicationsModel();
				sub.setPub_id(pub_id);
				sub.setTitle(title);
				sub.setPrice(price);
				sub.setType(type);
				sub.setStock(stock);
				sub.setFrequency(frequency);
				pList.add(count, sub);
			}

		} catch (Exception e) {
			System.out.println("Error in loading Database");
		}
		return pList;
	}
	
	public void displayData() {


		pView.setTitles(pList.get(count).getTitle());
		pView.setType(pList.get(count).getType());
		pView.setPrice(Double.toString(pList.get(count).getPrice()));
		pView.setPubID(Integer.toString(pList.get(count).getPub_id()));
		pView.setStock(Integer.toString(pList.get(count).getStock()));
		pView.setFrequency(pList.get(count).getFrequency());
	}
	
	class firstPublication implements ActionListener {

		public firstPublication() {
			pView.addFirstActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = 0;
			displayData();
		}

	}
	
	class previousPublication implements ActionListener {

		public previousPublication() {
			pView.addPrevActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count > 0) {
				count--;
				System.out.println("Prev");
			}
			displayData();

		}

	}

	class lastPublication implements ActionListener {

		public lastPublication() {
			pView.addLastActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = pList.size() - 1;
			displayData();
		}

	}

	class nextPublication implements ActionListener {

		public nextPublication() {
			pView.addNextActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count < pList.size() - 1) {
				count++;
				System.out.println("Next");

			}
			displayData();
		}

	}

	class addPublication implements ActionListener {

		public addPublication() {
			pView.addAddActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			pView.dispose();
			new NewPublicationsController();
		}

	}

	class deletePublication implements ActionListener {

		public deletePublication() {
			pView.addDeleteActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int pub_id = Integer.parseInt(pView.getPubID());
			String str = "Delete From PUBLICATION where pub_id= " + pub_id + ";";
			try {
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(pView, "Sucessfully Deleted!!!", null, JOptionPane.PLAIN_MESSAGE);
				displayData();
			} catch (SQLException sqle) {
				System.out.println(sqle.getMessage());
				JOptionPane.showMessageDialog(pView, "Unsucessfully added!!!", null, JOptionPane.PLAIN_MESSAGE);

			}
		}
	}

}
