package mvc.Delivery;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import mvc.Database;

public class DeliveryController {

	private DeliveryView dView = new DeliveryView();
	private Database db = new Database();
	ArrayList<DeliveryModel> dList = new ArrayList<DeliveryModel>();
	private int count;

	public DeliveryController() {

		onload();
		dView.init();
		new firstDelivery();
		new previousDelivery();
		new lastDelivery();
		new nextDelivery();
		new addDelivery();
		new deleteDelivery();

	}

	public void onload() {

		String str = "select customers.cus_id, customers.reg_id, customers.first_name, customers.last_name, customers.address_line1, customers.eircode, delivery.delivery_id, delivery.del_date, subscribes.sub_id,publication.title, publication.price, publication.type,runs.driver_id from customers, delivery, subscribes, publication, invoice, runs where customers.cus_id = delivery.cus_id and customers.cus_id = subscribes.cus_id and subscribes.pub_id = publication.pub_id and runs.reg_id = customers.reg_id;";
		try {
			Database.rs = Database.stmt.executeQuery(str);
			while (Database.rs.next()) {

				int cus_id = Database.rs.getInt("cus_id");
				int reg_id = Database.rs.getInt("reg_id");
				String first_name = Database.rs.getString("first_name");
				String last_name = Database.rs.getString("last_name");
				String address_line1 = Database.rs.getString("address_line1");
				String eircode = Database.rs.getString("eircode");
				int del_id = Database.rs.getInt("delivery_id");
				int sub_id = Database.rs.getInt("sub_id");
				int driver_id = Database.rs.getInt("driver_id");
				String title = Database.rs.getString("title");
				double price = Database.rs.getDouble("price");
				String type = Database.rs.getString("type");
				boolean delivered = false;
				Date del_date = Database.rs.getDate("del_date");
				DeliveryModel del = new DeliveryModel();
				del.setCus_id(cus_id);
				del.setReg_id(reg_id);
				del.setFirst_name(first_name);
				del.setLast_name(last_name);
				del.setAddress_line1(address_line1);
				del.setEircode(eircode);
				del.setDel_id(del_id);
				del.setSub_id(sub_id);
				del.setDriver_id(driver_id);
				del.setTitles(title);
				del.setPrice(price);
				del.setType(type);
				del.setDelivered(delivered);
				del.setDel_date(del_date);
				dList.add(del);
				count++;
			}
			displayData();

		} catch (Exception e) {
			System.out.println("Error in loading Database");
		}
	}

	public void displayData() {
		dView.setcusID(Integer.toString(dList.get(count).getCus_id()));
		dView.setFirstName(dList.get(count).getFirst_name());
		dView.setLastName(dList.get(count).getLast_name());
		dView.setAddress1(dList.get(count).getAddress_line1());
		dView.setEircode(dList.get(count).getEircode());
		dView.setSubID(Integer.toString(dList.get(count).getSub_id()));
		dView.setTitles(dList.get(count).getTitle());
		dView.setType(dList.get(count).getType());
		dView.setPrice(Double.toString(dList.get(count).getPrice()));
		dView.setDelID(Integer.toString(dList.get(count).getDel_id()));
		dView.setDriverID(Integer.toString(dList.get(count).getDriver_id()));
		dView.setRegID(Integer.toString(dList.get(count).getReg_id()));
		dView.setDelDate((dList.get(count).getDel_date().toString()));
	}

	class firstDelivery implements ActionListener {

		public firstDelivery() {
			dView.addFirstActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = 0;
			onload();
			displayData();
		}

	}

	class previousDelivery implements ActionListener {

		public previousDelivery() {
			dView.addPrevActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count > 0) {
				count--;
			}

			displayData();
		}

	}

	class lastDelivery implements ActionListener {

		public lastDelivery() {
			dView.addLastActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = dList.size() - 1;
		}

	}

	class nextDelivery implements ActionListener {

		public nextDelivery() {
			dView.addNextActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count < dList.size() - 1) {
				count++;
			}
			displayData();
		}

	}

	class addDelivery implements ActionListener {

		public addDelivery() {

			dView.addAddActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			dView.dispose();
			new NewDeliveryController();
		}

	}

	class deleteDelivery implements ActionListener {

		public deleteDelivery() {
			dView.addDeleteActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int del_id = Integer.parseInt(dView.getDelID());
			String str = "Delete From Delivery where delivery_id= " + del_id + ";";
			try {
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(dView, "Sucessfully Deleted!!!", null, JOptionPane.PLAIN_MESSAGE);
				displayData();
			} catch (SQLException sqle) {
				System.out.println();
				JOptionPane.showMessageDialog(dView, "Unsucessfully added!!!" + sqle.getMessage(), null,
						JOptionPane.PLAIN_MESSAGE);

			}

		}

	}

}
