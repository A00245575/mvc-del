package mvc.Delivery;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class NewDeliveryView extends JFrame {
	
	private JLabel cusID = new JLabel("Customer ID:");
	private JLabel del_date = new JLabel("Date(YYYY-MM-DD):");
	
	private JTextField cusID_data = new JTextField("");
	private JTextField ordDate_data = new JTextField("");
	
	private JButton insert = new JButton("Insert");
	private JButton ret = new JButton("Return");
	
	private Container contentPane = this.getContentPane();
	
	public void addInsertActionListener (ActionListener l)
    {
        insert.addActionListener(l);
    }
 
	public void addReturnActionListener (ActionListener l)
    {
        ret.addActionListener(l);
    }
 
	public NewDeliveryView()
	{
		this.setSize(800, 600);
        this.setTitle("Add new Delivery");
        this.setLayout(new GridLayout(0, 2));
	}
	
	public void init()
	{
		contentPane.add(cusID);
		contentPane.add(cusID_data);
		//contentPane.add(del_date);
		//contentPane.add(ordDate_data);
		contentPane.add(insert);
		contentPane.add(ret);
		this.setVisible(true);
	}
 

	public String getCusID()
	{
		return cusID_data.getText();
	}
	public void setcusID(String s)
	{
		cusID_data.setText(s);	
	}
	
	public String getDelDate()
	{
		return ordDate_data.getText();
	}
	public void setDelDate(String s)
	{
		ordDate_data.setText(s);	
	}
}
