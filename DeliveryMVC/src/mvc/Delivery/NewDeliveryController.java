package mvc.Delivery;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Statement;

import mvc.Database;

public class NewDeliveryController {

	private NewDeliveryView dView = new NewDeliveryView();
	private Database db = new Database();

	public NewDeliveryController() {
		dView.init();
		new addDelivery();
		new returns();
	}

	public static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days); // minus number would decrement the days
		return cal.getTime();
	}

	class addDelivery implements ActionListener {

		public addDelivery() {
			dView.addInsertActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			//newDelivery();
			int cus_id = Integer.parseInt(dView.getCusID());
			java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
			addDays(sqlDate, 1);
			String str = "INSERT INTO Delivery VALUES (null," + cus_id + ",'" + sqlDate + "');";
			try{
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(dView, "Sucessfully added!!!", "New Delivery", JOptionPane.PLAIN_MESSAGE);
			}
			catch(SQLException e1)
			{
				JOptionPane.showMessageDialog(dView, "Unsucessfully added!!!", "New Delivery", JOptionPane.PLAIN_MESSAGE);
			}
		}

	}

	class returns implements ActionListener {
		public returns() {
			dView.addReturnActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent l) {
			// TODO Auto-generated method stub
			dView.dispose();
			new DeliveryController();
		}

	}
}
