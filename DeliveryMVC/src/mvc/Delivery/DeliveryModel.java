package mvc.Delivery;

import java.sql.Date;

public class DeliveryModel {
	
	//
	private  int cus_id;
	private int reg_id ;
	private String first_name;
	private String last_name;
	private String address_line1;
	private String eircode;
	private int delivery_id;
	private int sub_id;
	private String title;
	private double price;
	private String type ;
	private int driver_id;
	private boolean delivered;
	private Date del_date;

	public Date getDel_date() {
		return del_date;
	}

	public void setDel_date(Date ord_date) {
		this.del_date = ord_date;
	}
	
	
	//setters
	public void setCus_id(int s)
    {
		cus_id = s;
    }

    public void setReg_id(int s)
    {
    	reg_id = s;
    }
    
    public void setFirst_name(String s)
    {
    	first_name = s;
    }
    
    public void setLast_name(String s)
    {
    	last_name = s;
    }
    
    public void setAddress_line1(String s)
    {
    	address_line1 = s;
    }
    
    public void setEircode(String s)
    {
    	eircode = s;
    }
    
    public void setDel_id(int s)
    {
		delivery_id = s;
    }
    
    public void setSub_id(int s)
    {
		sub_id = s;
    }
    
    public void setTitles(String s)
    {
    	title = s;
    }
    
    public void setPrice(double s)
    {
    	price = s;
    }
    
    public void setType(String s)
    {
    	type = s;
    }
    
    public void setDriver_id(int s)
    {
    	driver_id = s;
    }
    
    public void setDelivered(boolean s)
    {
    	delivered = s;
    }
    
    
    
    //getters
    public int getCus_id()
    {
    	return cus_id;
    }
    
    public int getReg_id()
    {
    	
    	return reg_id;
    }
    
    public String getFirst_name()
    {
    	return first_name;
    }
    
    public String getLast_name()
    {
    	return last_name;
    }
    
    public String getAddress_line1()
    {
    	return address_line1;
    }
    
    public String getEircode()
    {
    	return eircode;
    }
    
    public int getDel_id()
    {
    	
    	return delivery_id;
    }
    
    public int getSub_id()
    {
    	return sub_id;
    }
    
    public String getTitle()
    {
    	return title;
    }
    
    public int getDriver_id()
    {
    	return driver_id;
    }
    
    public double getPrice()
    {
    	return price;
    }
    
    public String getType()
    {
    	return type;
    }
    
    public boolean getDelivered()
    {
    	return delivered;
    }

}
