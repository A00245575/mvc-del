package mvc.Subscription;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import mvc.Database;

public class SubscriptionController {

	private SubscriptionView sView = new SubscriptionView();
	private Database db = new Database();
	ArrayList<SubscriptionModel> sList = new ArrayList<SubscriptionModel>();
	private int count = 0;

	public SubscriptionController() {
		onload();
		sView.init();
		new firstSubscription();
		new previousSubscription();
		new lastSubscription();
		new nextSubscription();
		new addSubscription();
		new deleteSubscription();
	}

	public ArrayList<SubscriptionModel> onload() {

		
		String str = "select customers.cus_id, customers.first_name, customers.last_name, customers.address_line1, customers.eircode, subscribes.sub_id, publication.frequency, subscribes.ord_date, publication.pub_id, publication.title, publication.price, publication.type from customers, subscribes, publication where customers.cus_id = subscribes.cus_id and subscribes.pub_id = publication.pub_id;";
		try {
			Database.rs = Database.stmt.executeQuery(str);
			while (Database.rs.next()) {

				int cus_id = Database.rs.getInt("cus_id");
				String first_name = Database.rs.getString("first_name");
				String last_name = Database.rs.getString("last_name");
				String address_line1 = Database.rs.getString("address_line1");
				String eircode = Database.rs.getString("eircode");
				int sub_id = Database.rs.getInt("sub_id");
				int pub_id = Database.rs.getInt("pub_id");
				String title = Database.rs.getString("title");
				double price = Database.rs.getDouble("price");
				String type = Database.rs.getString("type");
				String freq = Database.rs.getString("frequency");
				Date ord_date = Database.rs.getDate("ord_date");
				SubscriptionModel sub = new SubscriptionModel();
				sub.setCus_id(cus_id);
				sub.setFirst_name(first_name);
				sub.setLast_name(last_name);
				sub.setAdd_line1(address_line1);
				sub.setEircode(eircode);
				sub.setSub_id(sub_id);
				sub.setPub_id(pub_id);
				sub.setTitle(title);
				sub.setPrice(price);
				sub.setType(type);
				sub.setFrequency(freq);
				sub.setOrd_date(ord_date);
				sList.add(sub);
			}

		} catch (Exception e) {
			System.out.println("Error in loading Database");
		}
		return sList;
	}

	public void displayData() {

		sView.setcusID(Integer.toString(sList.get(count).getCus_id()));
		sView.setFirstName(sList.get(count).getFirst_name());
		sView.setLastName(sList.get(count).getLast_name());
		sView.setAddress1(sList.get(count).getAdd_line1());
		sView.setEircode(sList.get(count).getEircode());
		sView.setSubID(Integer.toString(sList.get(count).getSub_id()));
		sView.setTitles(sList.get(count).getTitle());
		sView.setType(sList.get(count).getType());
		sView.setPrice(Double.toString(sList.get(count).getPrice()));
		sView.setPubID(Integer.toString(sList.get(count).getPub_id()));
		sView.setFreq(sList.get(count).getFrequency());
		sView.setOrdDate((sList.get(count).getOrd_date().toString()));

	}

	class firstSubscription implements ActionListener {

		public firstSubscription() {
			sView.addFirstActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = 0;
			displayData();
		}

	}

	class previousSubscription implements ActionListener {

		public previousSubscription() {
			sView.addPrevActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count > 0) {
				count--;
				System.out.println("Prev");
			}
			displayData();

		}

	}

	class lastSubscription implements ActionListener {

		public lastSubscription() {
			sView.addLastActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			count = sList.size() - 1;
			displayData();
		}

	}

	class nextSubscription implements ActionListener {

		public nextSubscription() {
			sView.addNextActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (count < sList.size() - 1) {
				count++;
				System.out.println("Next");

			}
			displayData();
		}

	}

	class addSubscription implements ActionListener {

		public addSubscription() {
			sView.addAddActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			sView.dispose();
			new NewSubscriptionController();
		}

	}

	class deleteSubscription implements ActionListener {

		public deleteSubscription() {
			sView.addDeleteActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int sub_id = Integer.parseInt(sView.getSubID());
			String str = "Delete From SUBSCRIBES where sub_id= " + sub_id + ";";
			try {
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(sView, "Sucessfully Deleted!!!", null, JOptionPane.PLAIN_MESSAGE);
				displayData();
			} catch (SQLException sqle) {
				System.out.println(sqle.getMessage());
				JOptionPane.showMessageDialog(sView, "Unsucessfully added!!!", null, JOptionPane.PLAIN_MESSAGE);

			}
		}
	}

}
