package mvc.Subscription;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import javax.swing.JOptionPane;

import mvc.Database;


public class NewSubscriptionController {
	
	private NewSubscriptionView sView = new NewSubscriptionView();
    private Database db = new Database();

    public NewSubscriptionController()
    {
    	sView.init();
    	new addSubscription();
    	new returns();
    }
    
    class addSubscription implements ActionListener {
    	
    	public addSubscription()
    	{
    		sView.addInsertActionListener(this);
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int cus_id = Integer.parseInt(sView.getCusID());
			int pub_id = Integer.parseInt(sView.getPubID());
			java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
			
			String str = "INSERT INTO Subscribes VALUES (null,'" + cus_id + "',"+ pub_id + ", '" +sqlDate+"');";
			try
			{
				Database.stmt.executeUpdate(str);
				JOptionPane.showMessageDialog(sView, "Sucessfully added!!!", "New Subscription", JOptionPane.PLAIN_MESSAGE);

			}
			catch(SQLException sqle)
			{
				System.out.println(sqle.getMessage());
				JOptionPane.showMessageDialog(sView, "Unsucessfully added!!!", "New Subscription", JOptionPane.PLAIN_MESSAGE);

			}
		}
    }
    class returns implements ActionListener
	{
		public returns()
		{
			sView.addReturnActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent l) {
			// TODO Auto-generated method stub
			sView.dispose();
			new SubscriptionController();
		}
		
	}
}
