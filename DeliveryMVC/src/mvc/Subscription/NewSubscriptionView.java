package mvc.Subscription;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class NewSubscriptionView extends JFrame {

	private JLabel cusID = new JLabel("Customer ID:");
	private JLabel pubID = new JLabel("Publication ID:");
	
	private JLabel ord_date = new JLabel("Date(YYYY-MM-DD):");

	private JTextField cusID_data = new JTextField("");
	private JTextField pubID_data = new JTextField("");
	
	private JTextField ordDate_data = new JTextField("");

	private JButton insert = new JButton("Insert");
	private JButton ret = new JButton("Return");

	private Container contentPane = this.getContentPane();

	public void addInsertActionListener(ActionListener l) {
		insert.addActionListener(l);
	}

	public void addReturnActionListener(ActionListener l) {
		ret.addActionListener(l);
	}

	public NewSubscriptionView() {
		this.setSize(800, 600);
		this.setTitle("Add new Subscription");
		this.setLayout(new GridLayout(0, 2));
	}

	public void init() {
		contentPane.add(cusID);
		contentPane.add(cusID_data);
		contentPane.add(pubID);
		contentPane.add(pubID_data);
		
		//contentPane.add(ord_date);
		//contentPane.add(ordDate_data);
		contentPane.add(insert);
		contentPane.add(ret);
		this.setVisible(true);
	}

	public String getOrdDate() {
		return ordDate_data.getText();
	}

	public void setOrdDate(String s) {
		ordDate_data.setText(s);
	}

	public String getCusID() {
		return cusID_data.getText();
	}

	public void setcusID(String s) {
		cusID_data.setText(s);
	}

	public String getPubID() {
		return pubID_data.getText();
	}

	public void setPubID(String s) {
		pubID_data.setText(s);
	}

	
}
